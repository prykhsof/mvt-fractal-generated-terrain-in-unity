
using System;

using UnityEngine;
using Random = UnityEngine.Random;

public class DiamondSquare : MonoBehaviour
{
    public int divisions; // number of faces
    public float height;
    public float size;
    
    private Vector3[] _verts; //array of all vertex
    private int _vertCount; // count of vertex

    private Vector2[] _uvs; // count of UVs
    private int[] _triangles; // array of all triangles
    
    private Mesh mesh;
    
    

    // Start is called before the first frame update
    void Start()
    {
        mesh = new Mesh();
        MeshFilter filter = gameObject.GetComponent<MeshFilter>();
        if(!filter) gameObject.AddComponent<MeshFilter>();
        GetComponent<MeshFilter>().mesh = mesh;
        create_terrain();
    }


    void  create_terrain()
    {
        _vertCount = (int)Math.Pow(divisions + 1, 2); // number of vertices is 2^n + 1 
        _verts = new Vector3[_vertCount]; // initiate the verts
        _uvs = new Vector2[_vertCount]; // initiate the UVs
        _triangles = new int [(int)Math.Pow(divisions, 2) * 6]; // numbers of triangles
        // in the start is 2 * division^2
        // in unity triangles are defined by 3 numbers 
        // so result is 3 * 2 * division^2


        //_____create the mesh______________________________________________________________________
        int inx = 0;
        for (int  offset = 0, x = 0; x <= divisions; x++)
        {
            for (int z = 0; z <= divisions; z++)
            {
                _verts[inx] = new Vector3(-0.5f*size+z*divisions, 0, 0.5f*size - x*divisions);
                _uvs[inx] = new Vector2((float)x / divisions, (float)z / divisions); 
                if (x<divisions && z <divisions)  create_triangles(inx, ref offset);
                inx++;
               
            }
        }
        
       //_____base_vertices______________________________________________________________________
       _verts[0].y = Random.Range(-height, height);
       _verts[divisions].y = Random.Range(-height, height);
       _verts[_verts.Length-1].y = Random.Range(-height, height);
       _verts[_verts.Length-divisions-1].y = Random.Range(-height, height);

       //_____diamond_square_algorithm______________________________________________________________________
       int iterations = (int)Mathf.Log(divisions, 2);
       int numSquares = 1;
       int squareSize = divisions;
       for (int i = 0; i < iterations; i++)
       {
           int row = 0;
           for (int j = 0; j < numSquares; j++)
           {
               int col = 0;
               for (int k = 0; k < numSquares; k++)
               {
                   diamond_square(row, col, squareSize, height);
                   col += squareSize;
               }
               row += squareSize;
               
           }

           numSquares *= 2;
           squareSize /= 2;
           height *= 0.5f;

       }
       
       
      
    }
    
    void diamond_square(int row, int col, int size, float offset)
    {
        //_____diamond__________________________________________________________________
        int halfSize = (int)(size * 0.5f);
        int topLeft = row * (divisions + 1) + col;
        int botLeft = (row + size) * (divisions + 1) + col;
        int mid = (row + halfSize) * (divisions + 1) + col + halfSize;
        _verts[mid].y = (_verts[topLeft].y + _verts[topLeft + size].y 
                                           + _verts[botLeft].y 
                                           + _verts[botLeft+size].y) * 0.25f 
                                           + Random.Range(-offset, offset);
        //_____squares__________________________________________________________________
        _verts[topLeft + halfSize].y = (_verts[topLeft].y 
                                        + _verts[topLeft + size].y 
                                        + _verts[mid].y)/3
                                        + Random.Range(-offset, offset);
        
        _verts[mid-halfSize].y = (_verts[topLeft].y 
                                  + _verts[botLeft + size].y 
                                  + _verts[mid].y)/3
                                  + Random.Range(-offset, offset);
        _verts[mid+halfSize].y =  (_verts[topLeft+size].y 
                                    + _verts[botLeft + size].y 
                                    + _verts[mid].y)/3
                                   + Random.Range(-offset, offset);
        _verts[botLeft+halfSize].y =  (_verts[botLeft].y 
                                    + _verts[botLeft + size].y 
                                    + _verts[mid].y)/3
                                   + Random.Range(-offset, offset);
    }
    void create_triangles(int vert, ref int offset)
    {
        int top = vert;
        int bottom = vert + divisions +1;
       
    _triangles[offset++] = top;
    _triangles[offset++] = top +1;
    _triangles[offset++] = bottom;
    
    _triangles[offset++] = bottom;
    _triangles[offset++] = top +1;
    _triangles[offset++] = bottom  + 1;
  
    }

    // Update is called once per frame
    private void Update()
    {
        update_terrain();
    }

    void update_terrain()
    {
        mesh.Clear();

        mesh.vertices = _verts;
        mesh.triangles = _triangles;
        mesh.uv = _uvs;
        
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }
    
}
