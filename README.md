# MVT. Fractal generated terrain in Unity
### Autor: Sofiia Prykhach (prykhsof)

<img
  src="https://gitlab.fit.cvut.cz/prykhsof/mvt-fractal-generated-terrain-in-unity/-/raw/master/terrain_example.png"
  style="display: inline-block; margin: 0 auto; max-width: 300px">

**Diamond** **Square** **Algorithm**  je algoritmus pro generování výškových map v počítačových hrách a grafice.
Algoritmus funguje tak, že nejprve inicializuje výškovou mapu s náhodnými hodnotami v rozích a poté opakovaně aplikuje diamond a square kroky, přičemž průměruje hodnoty sousedy a přidává náhodný šum. 
Tímto způsobem se postupně vytváří detailnější a realističtější výšková mapa.

Tento kód  používá Diamond Square Algorithm v Unity pro generaci objektu s názvem "**mountain**".  Uživatel  může nastavit parametry požadované výšky (**height**),
velikosti generovaného pole(**size**) a počtu bodů (**divisions**). 
Tím umožňuje uživateli přizpůsobit výsledný vzhled krajiny jeho potřebám a představám. Kód byl napsán v jazyce C#.

Použili jsme textury, které byly zapůjčeny z bezplatného dodatečného balíčku **Terrain** **Sample** **Assets** v Unity. 

Všechny soubory projektu, které jsou používány v Unity, jsou uloženy ve složce  **diamond_square_landscape**.
  Hlavní kód je uloženy v **diamond_square_landscape/Assets/Scripts/DiamondSquare.cs**. 
  Scena s objektem je v **diamond_square_landscape/Assets/Scenes/SampleScene.unity**
 
